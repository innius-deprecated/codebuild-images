#!/bin/bash
                
# get the postman collection id for the service
curl -X GET https://api.getpostman.com/collections -H ""x-api-key:$POSTMAN_APIKEY"" 
# - curl -o postman_collections.json -X GET https://api.getpostman.com/collections -H ""x-api-key:$POSTMAN_APIKEY"" 
# - collection_id="$(cat ./postman_collections.json | jq --raw-output '.collections | map(select(.name == "'"$SERVICE_NAME"'"))[0].id')"                
# - curl -o postman_environments.json -X GET https://api.getpostman.com/environments -H ""x-api-key:$POSTMAN_APIKEY""  

# # get the environment id for local test run 
# - environment_id="$(cat ./postman_environments.json | jq --raw-output '.environments | map(select(.name == "'"${SERVICE_NAME}-local"'"))[0].id')"                                 

# - newman run "https://api.getpostman.com/collections/${collection_id}?apikey=${POSTMAN_APIKEY}" -e "https://api.getpostman.com/environments/${environment_id}?apikey=${POSTMAN_APIKEY}" -r cli  