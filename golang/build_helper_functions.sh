#!/bin/bash
export SERVICE_NAME=`echo "${CODEBUILD_INITIATOR##*/}"`
export COMMIT_SHA"$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | head -c 8)"      

function innius_prebuild {    
    echo $COMMIT_SHA > COMMIT_SHA                        
}

# golang services with local packages must be in the GOPATH 
function golang_pre_build {
    mkdir -p /go/src/bitbucket.org/to-increase/$SERVICE_NAME   
    cp * -r /go/src/bitbucket.org/to-increase/$SERVICE_NAME
    go get -t -d -v ./...      
}



function start_dynamodb {
    echo "Starting dynamodb as a detached background process at http://localhost:8000..."
    java -Djava.library.path=/tmp/DynamoDBLocal_lib -jar /opt/dynamodb-local/DynamoDBLocal.jar -inMemory &
    sleep 2
}

function start_mountebank {
    echo "Starting mountebank as a detached background process..."
    mb &
    sleep 2
}

function start_elasticmq {
    echo "Starting elasticmq (AWS SQS) as a detached background process..."
    java -jar /opt/elasticmq-server.jar &
    sleep 5
}

function start_kinesalite {
    echo "Starting kinesalite (AWS Kinesis) as a detached background process at http://localhost:4567..."
    kinesalite &
    sleep 2
}

function start_kinesalite_ssl {
    echo "Starting kinesalite [SSL] (AWS Kinesis) as a detached background process at https://localhost:4568..."
    kinesalite --ssl --port 4568 &
    sleep 2
}